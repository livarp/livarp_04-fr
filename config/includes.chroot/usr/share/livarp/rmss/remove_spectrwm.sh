#!/bin/bash
# spectrwm section

source ~/.bashrc
# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/human ]; then
    echo "ce script n'est pas accessible en session live" | dzen2 -x 0 -y 0 -fg 'white' -p 5 &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo "ce script demande les droits administrateurs - lancez-le en root" | dzen2 -x 0 -y 0 -bg 'orange' -fg 'white' -p 5 &
    exit
fi

# spectrwm stuff
# ----------------------------------------------------------------------
echo ""
echo ""
echo -e "${red} vous êtes sur le point d'effacer la session spectrwm"
echo -en " voulez-vous continuer ? [O|n]$NC "
read a
if [ "$a" = "n" ] || [ "$a" = "N" ]; then
    echo ""
    echo "script avorté"
    sleep 4s && exit 0
else
    echo ""
    echo " ---élimination de la session spectrwm------------------------"
    echo ""
    echo " élimination des fichiers système."
    echo ""
    sleep 2s
    apt-get autoremove --purge -y spectrwm
    rm /usr/local/bin/spectrwm_keys.sh
    rm -R -f /etc/skel/.spectrwm.conf
    rm /etc/skel/bin/start/spectrwm_start.sh
    rm /etc/skel/bin/spectrwm_status.sh
    echo ""
    echo " élimination des fichiers utilisateur."
    echo ""
    sleep 2s
    rm $HOME/bin/remove-sessions/rm-spectrwm-session
    rm $HOME/bin/start/spectrwm_start.sh
    rm $HOME/.spectrwm.conf
    rm $HOME/bin/spectrwm_status.sh
    echo ""
    echo -e "${cyan} session spectrwm éliminée$NC "
    echo ""
    sleep 2s
fi
#eof--------------------------------------------------------------------
