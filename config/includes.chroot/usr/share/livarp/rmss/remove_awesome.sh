#!/bin/bash
# awesome section

source ~/.bashrc
# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/human ]; then
    echo "ce script n'est pas accessible en session live" | dzen2 -x 0 -y 0 -fg 'white' -p 5 &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo "ce script demande les droits administrateurs - lancez-le en root" | dzen2 -x 0 -y 0 -bg 'orange' -fg 'white' -p 5 &
    exit
fi

# awesome stuff
# ----------------------------------------------------------------------
echo ""
echo ""
echo -e "${red} vous êtes sur le point d'effacer la session awesome"
echo -en " voulez-vous continuer ? [O|n]$NC "
read a
if [ "$a" = "n" ] || [ "$a" = "N" ]; then
    echo ""
    echo "script avorté"
    sleep 4s && exit 0
else
    echo ""
    echo " ---élimination d'awesome-------------------------------------"
    echo ""
    echo " élimination des fichiers système."
    echo ""
    sleep 2s
    apt-get autoremove --purge -y awesome awesome-extra
    rm /etc/skel/.conky/conkyrc_awesome
    rm /etc/skel/bin/start/awesome_start.sh
    rm -R -f /etc/skel/.config/awesome
    echo ""
    echo " élimination des fichiers utilisateur."
    echo ""
    sleep 2s
    rm $HOME/bin/start/awesome_start.sh
    rm $HOME/bin/remove-sessions/rm-awesome-session
    rm $HOME/.conky/conkyrc_awesome
    rm -R -f $HOME/.config/awesome
    echo ""
    echo -e "${cyan} session awesome effacée$NC "
    echo ""
    sleep 2s
fi
#eof--------------------------------------------------------------------
