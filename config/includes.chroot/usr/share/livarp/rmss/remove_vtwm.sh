#!/bin/bash
# vtwm section

source ~/.bashrc
# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/human ]; then
    echo "ce script n'est pas accessible en session live" | dzen2 -x 0 -y 0 -fg 'white' -p 5 &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo "ce script demande les droits administrateurs - lancez-le en root" | dzen2 -x 0 -y 0 -bg 'orange' -fg 'white' -p 5 &
    exit
fi

# vtwm stuff
# ----------------------------------------------------------------------
echo ""
echo ""
echo -e "${red} vous êtes sur le point d'effacer la session vtwm"
echo -en " voulez-vous continuer ? [O|n]$NC "
read a
if [ "$a" = "n" ] || [ "$a" = "N" ]; then
    echo ""
    echo "script avorté"
    sleep 4s && exit 0
else
    echo ""
    echo " ---élimination de la session vtwm----------------------------"
    echo ""
    echo " élimination des fichiers système."
    echo ""
    sleep 2s
    apt-get autoremove --purge -y vtwm
    rm /usr/local/bin/vtwm_keys.sh
    rm -R -f /etc/skel/.vtwmrc
    rm /etc/skel/bin/start/vtwm_start.sh
    rm /etc/skel/.conky/conkyrc_vtwm
    echo ""
    echo " élimination des fichiers utilisateur."
    echo ""
    sleep 2s
    rm $HOME/bin/remove-sessions/rm-vtwm-session
    rm $HOME/bin/start/vtwm_start.sh
    rm $HOME/.vtwmrc
    rm $HOME/.conky/conkyrc_vtwm
    echo ""
    echo -e "${cyan} session vtwm éliminée$NC "
    echo ""
    sleep 2s
fi
#eof--------------------------------------------------------------------
