#!/bin/bash
# openbox section

source ~/.bashrc
# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/human ]; then
    echo "ce script n'est pas accessible en session live" | dzen2 -x 0 -y 0 -fg 'white' -p 5 &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo "ce script demande les droits administrateurs - lancez-le en root" | dzen2 -x 0 -y 0 -bg 'orange' -fg 'white' -p 5 &
    exit
fi

# openbox stuff
# ----------------------------------------------------------------------
echo ""
echo ""
echo -e " vous êtes sur le point d'effacer la session openbox"
echo -en " voulez-vous continuer ? [O|n]$NC "
read a
if [ "$a" = "n" ] || [ "$a" = "N" ]; then
    echo ""
    echo "script avorté"
    sleep 4s && exit 0
else
    echo ""
    echo " ---élimination de la session openbox-------------------------"
    echo ""
    echo " élimination des fichiers système."
    echo ""
    sleep 2s
    apt-get autoremove --purge -y openbox obconf obmenu
    rm -R -f /etc/skel/.config/openbox
    rm /etc/skel/.conky/conkyrc_openbox
    echo ""
    echo " élimination des fichiers utilisateur."
    echo ""
    sleep 2s
    rm $HOME/bin/remove-sessions/rm-openbox-session
    rm -R -f $HOME/.config/openbox
    rm $HOME/.conky/conkyrc_openbox
    echo ""
    echo -e "${cyan} session openbox éliminée$NC "
    echo ""
    sleep 2s
fi
#eof--------------------------------------------------------------------
