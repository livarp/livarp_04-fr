#!/bin/bash
# dwm section

source ~/.bashrc
# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/human ]; then
    echo "ce script n'est pas accessible en session live" | dzen2 -x 0 -y 0 -fg 'white' -p 5 &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo "ce script demande les droits administrateurs - lancez-le en root" | dzen2 -x 0 -y 0 -bg 'orange' -fg 'white' -p 5 &
    exit
fi

# dwm stuff
# ----------------------------------------------------------------------
echo ""
echo ""
echo -e "${red} vous êtes sur le point d'effacer les sessions dwm et dwm_reloaded"
echo -en " voulez-vous continuer ? [O|n]$NC "
read a
if [ "$a" = "n" ] || [ "$a" = "N" ]; then
    echo ""
    echo "script avorté"
    sleep 4s && exit 0
else
    echo ""
    echo " ---élimination des sessions dwm------------------------------"
    echo ""
    echo " élimination des fichiers système."
    echo ""
    sleep 2s
    apt-get autoremove --purge -y dwm
    rm /usr/local/bin/dwm
    rm /usr/local/bin/dwm_keys.sh
    rm /etc/skel/.conky/conkyrc_dwm
    rm /etc/skel/bin/start/dwm_start.sh
    rm /usr/share/livarp/sources/dwm_reloaded_l4.tgz
    echo ""
    echo " élimination des fichiers utilisateur."
    echo ""
    sleep 2s
    rm $HOME/bin/remove-sessions/rm-dwm-session
    rm $HOME/bin/start/dwm_start.sh
    rm $HOME/.conky/conkyrc_dwm
    echo ""
    echo -e "${cyan} sessions dwm éliminées$NC "
    echo ""
    sleep 2s
fi
#eof--------------------------------------------------------------------
