#!/bin/bash
# fluxbox section

source ~/.bashrc
# this script is not available in live-session
# ----------------------------------------------------------------------
if [ -d /home/human ]; then
    echo "ce script n'est pas accessible en session live" | dzen2 -x 0 -y 0 -fg 'white' -p 5 &
    exit
fi

# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo "ce script demande les droits administrateurs - lancez-le en root" | dzen2 -x 0 -y 0 -bg 'orange' -fg 'white' -p 5 &
    exit
fi

# fluxbox stuff
# ----------------------------------------------------------------------
echo ""
echo ""
echo -e "${red} vous êtes sur le point d'effacer la session fluxbox"
echo -en " voulez-vous continuer ? [O|n]$NC "
read a
if [ "$a" = "n" ] || [ "$a" = "N" ]; then
    echo ""
    echo "script avorté"
    sleep 4s && exit 0
else
    echo ""
    echo " ---élimination de la session fluxbox-------------------------"
    echo ""
    echo " élimination des fichiers système."
    echo ""
    sleep 2s
    apt-get autoremove -y fluxbox
    rm -R -f /usr/share/fluxbox
    rm -R -f /usr/local/bin/flumenu_datas
    rm /usr/local/bin/fluxmenu
    rm -R -f /etc/skel/.fluxbox
    rm /etc/skel/bin/start/fluxbox_start.sh
    rm /etc/skel/.conky/conkyrc_fluxbox
    echo ""
    echo " élimination des fichiers utilisateur."
    echo ""
    sleep 2s
    rm $HOME/bin/remove-sessions/rm-fluxbox-session
    rm $HOME/bin/start/fluxbox_start.sh
    rm -R -f $HOME/.fluxbox
    rm $HOME/.conky/conkyrc_fluxbox
    echo ""
    echo -e "${cyan} session fluxbox éliminée$NC "
    echo ""
    sleep 2s
fi
#eof--------------------------------------------------------------------
