#!/bin/bash
#####################################
## linux-libre installation script ##
##---------------------------------##
source ~/.bashrc
clear
# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo ""
    echo " mais ou est root..."
    echo " ce script a besoin des privileges administrateur"
    echo "   lancer ce script en tant que root."
    sleep 4s && exit 0
fi

# run only once
# ----------------------------------------------------------------------
if [ -e /usr/share/livarp/livarpfree ]; then
    echo ""
    echo " your system already includes linux-libre kernel"
    echo ""
    sleep 4s && exit 0
fi

# intro
# ----------------------------------------------------------------------
echo ""
echo -e "${cyan} welcome to linux-libre install script"
echo -e " -------------------------------------$NC"
echo ""
echo " ce script va : "
echo " - ajouter les archives jxself aux dépôts Debian"
echo " - installer le noyau 100% libre linux-libre"
echo ""
echo " vous aurez besoin d'une connexion internet active"
echo ""
echo " plus d'informations sur <http://jxself.org/linux-libre/>"
echo ""
echo -n " on continue ? [O|n] : "
read answer
if [ "$answer" = "n" ] || [ "$answer" = "N" ]; then
    echo ""
    echo " @+ $USER "
    echo ""
    sleep 2s
    exit 0
else
    # internet test
    # ------------------------------------------------------------------
    echo " Test de la connexion internet ..."
    sleep 2s
    echo ""
    IS=`/bin/ping -c 1 ftp.de.debian.org | grep -c "64 bytes"`
    if [ "$IS" -lt "1" ]; then
        until [ "$CONT" != "" ]; do
        echo ""
        IS=`/bin/ping -c 1 ftp.de.debian.org | grep -c "64 bytes"`
        if [ "$IS" -lt "1" ]; then
            clear
            echo "  pas de connexion internet."
            echo ""
            echo "  Configurez votre connexion"
            echo "  puis pressez une touche pour continuer"
            echo "  ou \"q + Enter\" pour quitter."
            read a
            if [ "$a" = "q" ]; then
                clear
                echo " @+. "
                echo ""
                sleep 2s
                exit 0
            fi
        else
            CONT="pass"
        fi
        done
    fi
    clear
    echo "  connexion active..."
    echo ""
    # adding new repo to sources.list
    # ------------------------------------------------------------------
    echo " ajout des sorces jxself"
    echo ""
    echo "# linux-libre #" >> /etc/apt/sources.list
    echo "deb http://linux-libre.fsfla.org/pub/linux-libre/freesh/ freesh main" >> /etc/apt/sources.list
    sleep 2s
    # adding jxself keyring
    # ------------------------------------------------------------------
    echo " ajout de la clef jxself"
    echo ""
    wget http://linux-libre.fsfla.org/pub/linux-libre/freesh/archive-key.asc
    apt-key add archive-key.asc 
    rm archive-key.asc 
    sleep 2s
    # system update
    # ------------------------------------------------------------------
    echo " mise à jour du système"
    echo ""
    sleep 2s
    apt-get update
    # install linux-libre
    # ------------------------------------------------------------------
    echo " installation de linux-libre headers & kernel"
    echo ""
    sleep 2s
    apt-get install -y linux-libre32-headers linux-libre32
fi
# end of script
# ----------------------------------------------------------------------
echo "linux-libre kernel installed - do not remove" > /usr/share/livarp/livarpfree
echo ""
echo " le dernier noyau linux-libre est installé."
echo " youhou !!"
echo ""
echo " vous pouvez redémarrer sur un noyau 100% libre :)"
echo ""
echo -n " presser Enter pour quitter."
read anykey
exit 0
# eof ------------------------------------------------------------------
