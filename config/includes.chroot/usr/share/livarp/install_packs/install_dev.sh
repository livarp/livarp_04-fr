#!/bin/bash
#############################
## dev installation script ##
##-------------------------##
source ~/.bashrc
clear
# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo ""
    echo " mais ou est root..."
    echo " ce script a besoin des privileges administrateur"
    echo "   lancer ce script en tant que root."
    sleep 4s && exit 0
fi

# intro
# ----------------------------------------------------------------------
echo ""
echo -e "${cyan} welcome to dev-pack install script"
echo -e " ----------------------------------$NC"
echo ""
echo " ce script va : "
echo " - installer git"
echo " - installer les outils nécessaires a la compilation de dwm ou wmfs"
echo ""
echo " vous aurez besoin d'une connexion internet active"
echo ""
echo -n " on continue ? [O|n] : "
read answer
if [ "$answer" = "n" ] || [ "$answer" = "N" ]; then
    echo ""
    echo " @+ $USER "
    echo ""
    sleep 2s
    exit 0
else
    # internet test
    # ------------------------------------------------------------------
    echo " Test de la connexion internet ..."
    sleep 2s
    echo ""
    IS=`/bin/ping -c 1 ftp.de.debian.org | grep -c "64 bytes"`
    if [ "$IS" -lt "1" ]; then
        until [ "$CONT" != "" ]; do
        echo ""
        IS=`/bin/ping -c 1 ftp.de.debian.org | grep -c "64 bytes"`
        if [ "$IS" -lt "1" ]; then
            clear
            echo "  pas de connexion internet."
            echo ""
            echo "  Configurez votre connexion"
            echo "  puis pressez une touche pour continuer"
            echo "  ou \"q + Enter\" pour quitter."
            read a
            if [ "$a" = "q" ]; then
                clear
                echo " @+. "
                echo ""
                sleep 2s
                exit 0
            fi
        else
            CONT="pass"
        fi
        done
    fi
    clear
    echo "  connexion active..."
    echo ""
    # system update & dev installation
    # ------------------------------------------------------------------
    echo " mise à jour du système et installation du dev-pack"
    echo ""
    sleep 2s
    apt-get update
    apt-get install -y build-essential git-core libimlib2-dev libx11-dev libxinerama-dev autoconf automake libxft-dev
fi
# end of script
# ----------------------------------------------------------------------
echo ""
echo " dev-pack est installé."
echo " youhou !!"
echo ""
echo " vous pouvez compiler depuis les sources maintenant :)"
echo ""
echo -n " presser Enter pour quitter."
read anykey
exit 0
# eof ------------------------------------------------------------------
