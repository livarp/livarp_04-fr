#!/bin/bash
################################
## pidgin installation script ##
##----------------------------##
source ~/.bashrc
clear
# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo ""
    echo " mais ou est root..."
    echo " ce script a besoin des privileges administrateur"
    echo "   lancer ce script en tant que root."
    sleep 4s && exit 0
fi

# intro
# ----------------------------------------------------------------------
echo ""
echo -e "${cyan} welcome to pidgin install script"
echo -e " --------------------------------$NC"
echo ""
echo " ce script va : "
echo " - installer pidgin ... vraiment ???"
echo " - oui ... euh en fait non..."
echo ""
echo " vous pouvez le faire vous-même :)"
echo ""
echo " ouvrez un terminal et tapez : \"sudo apt-get install pidgin\" "
echo ""
echo " passez une bonne journée :)"
echo -n " presser Enter pour quitter."
read anykey
exit 0
# eof ------------------------------------------------------------------
