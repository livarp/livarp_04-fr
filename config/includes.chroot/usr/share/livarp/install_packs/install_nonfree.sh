#!/bin/bash
##################################
## non-free installation script ##
##------------------------------##
source ~/.bashrc
clear
# are you root ?
# ----------------------------------------------------------------------
if [ "`whoami`" != "root" ]; then
    echo ""
    echo " mais ou est root..."
    echo " ce script a besoin des privileges administrateur"
    echo "   lancer ce script en tant que root."
    sleep 4s && exit 0
fi

# run only once
# ----------------------------------------------------------------------
if [ -e /etc/apt/sources.list.free.bak ]; then
    echo ""
    echo " your system already includes non-free stuff"
    echo ""
    sleep 4s
    exit 0
fi

# intro
# ----------------------------------------------------------------------
echo ""
echo -e "${cyan} welcome to nonfree-stuff install script"
echo -e " ---------------------------------------$NC"
echo ""
echo " ce script va : "
echo " - ajouter les sources nonfree"
echo " - ajouter les sources deb-multimedia"
echo " - mettre à jour le système"
echo " - installer flashplugin-nonfree"
echo ""
echo " vous aurez besoin d'une connexion internet active"
echo ""
echo " avez-vous vraiment besoin des dépôts non-libres ?"
echo -n "[o|N] : "
read answer
if [ "$answer" = "n" ] || [ "$answer" = "N" ] || [ "$answer" = "" ]; then
    echo ""
    echo " free as in freedom... @+ $USER "
    echo ""
    sleep 2s
    exit 0
else
    # internet test
    # ------------------------------------------------------------------
    echo ""
    echo " Test de la connexion internet ..."
    sleep 2s
    echo ""
    IS=`/bin/ping -c 1 ftp.de.debian.org | grep -c "64 bytes"`
    if [ "$IS" -lt "1" ]; then
        until [ "$CONT" != "" ]; do
        echo ""
        IS=`/bin/ping -c 1 ftp.de.debian.org | grep -c "64 bytes"`
        if [ "$IS" -lt "1" ]; then
            clear
            echo "  pas de connexion internet."
            echo ""
            echo "  Configurez votre connexion"
            echo "  puis pressez une touche pour continuer"
            echo "  ou \"q + Enter\" pour quitter."
            read a
            if [ "$a" = "q" ]; then
                clear
                echo " @+. "
                echo ""
                sleep 2s
                exit 0
            fi
        else
            CONT="pass"
        fi
        done
    fi
    clear
    echo "  connexion active..."
    echo ""
    # query non-free sources.list
    # ------------------------------------------------------------------
    echo " récuperation du sources.list non-free"
    echo ""
    mv /etc/apt/sources.list /etc/apt/sources.list.free.bak
    cp /usr/share/livarp/sources.list.nonfree /etc/apt/sources.list
    sleep 2s
    # adding deb-multimedia keyring
    # ------------------------------------------------------------------
    echo ""
    echo " ajout de la clef deb-multimedia"
    echo ""
    sleep 2s
    apt-get update && apt-get install --allow-unauthenticated deb-multimedia-keyring
    # upgrade system
    # ------------------------------------------------------------------
    echo ""
    echo " mise à jour du système"
    echo ""
    sleep 2s
    apt-get dist-upgrade -y --force-yes
    # install flashplugin-nonfree
    # ------------------------------------------------------------------
    echo ""
    echo " installation de flashplugin-nonfree"
    echo ""
    sleep 2s
    apt-get --no-install-recommends install flashplugin-nonfree
fi
# end of script
# ----------------------------------------------------------------------
echo ""
echo " fin de l'installation des non-free."
echo -n " presser Enter pour quitter."
read anykey
exit 0
# eof ------------------------------------------------------------------
