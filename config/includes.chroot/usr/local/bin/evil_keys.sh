#! /bin/bash
# livarp_0.4 evil keybinds & shortcuts
######################################

## text colors ---------------------------------------------------------
red='\e[0;31m'
blue='\e[0;34m'
cyan='\e[0;36m'
green='\e[0;32m'
yellow='\e[0;33m'
## No Color
NC='\e[0m'
## background colors ---------------------------------------------------
RED='\e[41m'
BLUE='\e[44m'
CYAN='\e[46m'
GREEN='\e[42m'
YELLOW='\e[43m'
## output --------------------------------------------------------------
echo ""
echo -e "     ${green}livarp 0.4 : raccourcis evil$NC"
echo ""
echo " touche de modif par défaut : Mod = Ctrl + Alt"
echo ""
echo -e " ${RED} quitter evilwm     $NC Mod + Backspace"
echo -e " ${RED} quitter livarp     $NC Ctrl + Shift + Alt + q"
echo -e " ${RED} fermer le client   $NC Mod + esc"
echo ""
echo -e " ${GREEN} ouvrir un terminal   $NC Mod + Return"
echo ""
echo -e " ${BLUE} déplace le client            $NC Mod + h/j/k/l or Alt + B1"
echo -e " ${BLUE} place le client              $NC Mod + y/u/b/n"
echo -e " ${BLUE} redimensionne le client      $NC Mod + Shift + h/j/k/l or Alt + B2"
echo -e " ${BLUE} passe le client derrière     $NC Mod + insert or Alt + B3"
echo -e " ${BLUE} maximise/rétablit le client  $NC Mod + x"
echo -e " ${BLUE} maximisation verticale       $NC Mod + ="
echo -e " ${BLUE} infos sur le client          $NC Mod + i"
echo -e " ${BLUE} fix client (visible partout) $NC Mod + f"
echo ""
echo -e " ${CYAN} bureau précédent/suivant $NC Mod + Left/Right"
echo -e " ${CYAN} client précédent/suivant $NC Alt + Tab"
echo -e " ${CYAN} aller au tag 'n'         $NC Mod + [1..n]"
echo ""
echo ""
echo -e " ${green} manuel complet avec [man evilwm]"
echo -en " ${green} taper Enter pour quitter..."
read anykey
exit 0
