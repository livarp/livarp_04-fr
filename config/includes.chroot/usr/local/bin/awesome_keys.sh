#! /bin/bash
# livarp_0.4 awesome keybinds & shortcuts
#########################################

## text colors ---------------------------------------------------------
red='\e[0;31m'
blue='\e[0;34m'
cyan='\e[0;36m'
green='\e[0;32m'
yellow='\e[0;33m'
## No Color
NC='\e[0m'
## background colors ---------------------------------------------------
RED='\e[41m'
BLUE='\e[44m'
CYAN='\e[46m'
GREEN='\e[42m'
YELLOW='\e[43m'
## output --------------------------------------------------------------
echo ""
echo -e "   ${green}raccourcis principaux - session awesome$NC"
echo -e "${green}definis dans ~/.config/awesome/config/keys.lua$NC"
echo ""
echo -e " ${RED} relancer awesome   $NC Ctrl + Alt + r"
echo -e " ${RED} quitter awesome    $NC Ctrl + Alt + q"
echo -e " ${RED} quitter le client  $NC Alt + q"
echo ""
echo -e " ${BLUE} menu awesome      $NC Alt + p"
echo -e " ${BLUE} lancer dmenu      $NC Alt + F2"
echo ""
echo -e " ${GREEN} ouvrir un terminal  $NC Alt + Return"
echo -e " ${GREEN} lancer rox-filer    $NC Alt + Shift + f"
echo -e " ${GREEN} lancer ranger       $NC Alt + f"
echo -e " ${GREEN} lancer firefox      $NC Alt + w"
echo -e " ${GREEN} lancer weechat      $NC Alt + i"
echo -e " ${GREEN} lecteur de musique  $NC Alt + m"
echo -e " ${GREEN} contrôle du volume  $NC Alt + v"
echo ""
echo -e " ${BLUE} libère le client       $NC Alt + Ctrl + space"
echo -e " ${BLUE} disposition précédente $NC Alt + Shift + space"
echo -e " ${BLUE} disposition suivante   $NC Alt + space"
echo ""
echo -e " ${CYAN} client préc/suivant  $NC Alt + k/j"
echo -e " ${CYAN} échange les clients  $NC Alt + Ctrl + k/j"
echo -e " ${CYAN} client urgent        $NC Alt + u"
echo -e " ${CYAN} tag préc/suivant     $NC Alt + Left/Right"
echo -e " ${CYAN} dernier tag          $NC Alt + Escape"
echo -e " ${CYAN} dernier client       $NC Alt + Tab"
echo ""
echo -e " ${CYAN} envoi au tag préc/suiv $NC Alt (+ Shift) + n"
echo -e " ${CYAN} aller au tag 'n'       $NC Alt + [1..n]"
echo ""
echo ""
echo -e " ${green} manuel complet disponible avec [man awesome]"
echo -e " ${green} liste complète des raccourcis dans le centre d'aide"
echo -en " ${green} taper Enter pour quitter..."
read anykey
exit 0
