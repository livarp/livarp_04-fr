#! /bin/bash
# livarp_0.4 dwm keybinds & shortcuts
#####################################

## text colors ---------------------------------------------------------
red='\e[0;31m'
blue='\e[0;34m'
cyan='\e[0;36m'
green='\e[0;32m'
yellow='\e[0;33m'
## No Color
NC='\e[0m'
## background colors ---------------------------------------------------
RED='\e[41m'
BLUE='\e[44m'
CYAN='\e[46m'
GREEN='\e[42m'
YELLOW='\e[43m'
## output --------------------------------------------------------------
echo ""
echo -e "     ${green}raccourcis dwm livarp 0.4$NC"
echo ""
echo -e " ${RED} relancer dwm     $NC Ctrl + Alt + r"
echo -e " ${RED} quitter dwm      $NC Ctrl + Alt + Backspace"
echo -e " ${RED} quitter livarp   $NC Ctrl + Shift + Alt + q"
echo -e " ${RED} fermer le client $NC Alt + q"
echo ""
echo -e " ${BLUE} lancer dmenu       $NC Alt + p"
echo -e " ${BLUE} scratchpad         $NC F12"
echo ""
echo -e " ${GREEN} ouvrir un terminal $NC Ctrl + Return"
echo -e " ${GREEN} ouvrir rox-filer   $NC Alt + Shift + r"
echo -e " ${GREEN} ouvrir ranger      $NC Alt + r"
echo -e " ${GREEN} ouvrir firefox     $NC Alt + w"
echo -e " ${GREEN} ouvrir vim         $NC Alt + e"
echo -e " ${GREEN} ouvrir geany       $NC Alt + Shift + e"
echo -e " ${GREEN} ouvrir weechat     $NC Alt + x"
echo -e " ${GREEN} lecteur de musique $NC Alt + z"
echo -e " ${GREEN} contrôle du volume  $NC Alt + v"
echo ""
echo -e " ${BLUE} tiling vertical    $NC Alt + t"
echo -e " ${BLUE} tiling horizontal  $NC Alt + s"
echo -e " ${BLUE} tiling fullscreen  $NC Alt + m"
echo -e " ${BLUE} pas de tiling      $NC Alt + f"
echo -e " ${BLUE} dernière disposition  $NC Alt + space"
echo -e " ${BLUE} change la disposition $NC Super + space"
echo ""
echo -e " ${CYAN} client précédent/suivant $NC Alt + k/j"
echo -e " ${CYAN} tag précédent/suivant    $NC Ctrl + Left/Right"
echo -e " ${CYAN} écran précédent/suivant  $NC Ctrl + Down/Up"
echo ""
echo -e " ${CYAN} envoyer à l'écran précédent/suivant $NC Ctrl + Shift + Down/Up"
echo -e " ${CYAN} envoyer au tag 'n'       $NC Alt + Shift + [1..n]"
echo -e " ${CYAN} aller au tag 'n'         $NC Alt + [1..n]"
echo -e " ${CYAN} client libre/tiled       $NC Alt + Shift + space"
echo ""
echo ""
echo -e " ${green} manuel complet [man dwm]"
echo -en " ${green} taper Enter pour quitter..."
read anykey
exit 0
