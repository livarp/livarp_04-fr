#! /bin/bash
# livarp_0.4 pekwm keybinds & shortcuts
#######################################

## text colors ---------------------------------------------------------
red='\e[0;31m'
blue='\e[0;34m'
cyan='\e[0;36m'
green='\e[0;32m'
yellow='\e[0;33m'
## No Color
NC='\e[0m'
## background colors ---------------------------------------------------
RED='\e[41m'
BLUE='\e[44m'
CYAN='\e[46m'
GREEN='\e[42m'
YELLOW='\e[43m'
## output --------------------------------------------------------------
echo ""
echo -e "    ${green}raccourcis clavier pekwm-livarp 0.4$NC"
echo ""
echo -e " ${RED} relancer pekwm     $NC Ctrl + Alt + r"
echo -e " ${RED} quitter pekwm      $NC Ctrl + Alt + Backspace"
echo -e " ${RED} quitter livarp     $NC Ctrl + Shift + Alt + q"
echo -e " ${RED} fermer le client   $NC Super + q"
echo ""
echo -e " ${BLUE} lancer dmenu         $NC Alt + d"
echo -e " ${BLUE} lancer une commande  $NC Super + d"
echo -e " ${BLUE} chercher une command $NC Super + v"
echo ""
echo -e " ${BLUE} menu              $NC Super + r"
echo -e " ${BLUE} menu fenêtres     $NC Super + w"
echo -e " ${BLUE} menu goto         $NC Super + l"
echo -e " ${BLUE} menu goto client  $NC Super + c"
echo -e " ${BLUE} cacher les menus  $NC Super + x"
echo ""
echo -e " ${GREEN} ouvrir un terminal  $NC Ctrl + Return"
echo -e " ${GREEN} ouvrir rox-filer    $NC Alt + Shift + r"
echo -e " ${GREEN} ouvrir ranger       $NC Alt + r"
echo -e " ${GREEN} ouvrir firefox      $NC Alt + Shift + w"
echo -e " ${GREEN} ouvrir vim editor   $NC Alt + e"
echo -e " ${GREEN} ouvrir geany        $NC Alt + Shift + e"
echo -e " ${GREEN} ouvrir weechat      $NC Alt + x"
echo -e " ${GREEN} ouvrir music player $NC Alt + z"
echo -e " ${GREEN} contrôler le volume $NC Alt + v"
echo ""
echo -e " ${BLUE} fermer le client       $NC Super + q"
echo -e " ${BLUE} maximiser oui/non      $NC Super + m"
echo -e " ${BLUE} pleinéecran oui/non    $NC Super + f"
echo -e " ${BLUE} enrouler oui/non       $NC Super + s"
echo -e " ${BLUE} minimiser oui/non      $NC Super + i"
echo ""
echo -e " ${CYAN} client préc/suivant        $NC Alt (+ Shift) + Tab"
echo -e " ${CYAN} client en tab préc/suivant $NC Alt (+ Shift) + Tab"
echo -e " ${CYAN} client de gauche/droite    $NC Super + Gauche/Droite"
echo -e " ${CYAN} client du haut/bas         $NC Super + Haut/Bas"
echo ""
echo -e " ${CYAN} aller au bureau de gauche/droite $NC Ctrl + Alt + Gauche/Droite"
echo -e " ${CYAN} aller au bureau du Haut/Bas      $NC Ctrl + Alt + Haut/Bas"
echo -e " ${CYAN} aller au bureau 'n'              $NC Super + [1..n]"
echo -e " ${CYAN} envoi au bureau 'n'              $NC Super + [F1..Fn]"
echo ""
echo -e " ${CYAN} envoi+va au bureau de gauche/droite $NC Ctrl + Shift + Alt + Gauche/Droite"
echo -e " ${CYAN} envoi+va au bureau du haut/bas      $NC Ctrl + Shift + Alt + Haut/Bas"
echo ""
echo ""
echo -e " ${green} liste complète des 'keychains' dans votre fichier ~/.pekwm/keys"
echo -en " ${green} presser Enter pour quitter..."
read anykey
exit 0
