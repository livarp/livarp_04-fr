#! /bin/bash
# livarp_0.4 spectrwm keybinds & shortcuts
##########################################

## text colors ---------------------------------------------------------
red='\e[0;31m'
blue='\e[0;34m'
cyan='\e[0;36m'
green='\e[0;32m'
yellow='\e[0;33m'
## No Color
NC='\e[0m'
## background colors ---------------------------------------------------
RED='\e[41m'
BLUE='\e[44m'
CYAN='\e[46m'
GREEN='\e[42m'
YELLOW='\e[43m'
## output --------------------------------------------------------------
echo ""
echo -e "  ${green}livarp 0.4 - raccourcis spectrwm$NC"
echo ""
echo -e " ${RED} relancer spectrwm    $NC Alt + q"
echo -e " ${RED} quitter spectrwm     $NC Alt + Shift + q"
echo -e " ${RED} fermer le client     $NC Alt + x"
echo -e " ${RED} tuer le client       $NC Alt + Shift + x"
echo ""
echo -e " ${BLUE} lancer dmenu        $NC Alt + p"
echo ""
echo -e " ${GREEN} ouvrir un terminal  $NC Alt + Shift + Return"
echo -e " ${GREEN} ouvrir rox-filer    $NC Super + Shift + r"
echo -e " ${GREEN} ouvrir ranger       $NC Super + r"
echo -e " ${GREEN} ouvrir firefox      $NC Super + w"
echo -e " ${GREEN} ouvrir vim          $NC Super + e"
echo -e " ${GREEN} ouvrir geany        $NC Super + Shift + e"
echo -e " ${GREEN} lecteur de musique  $NC Super + z"
echo -e " ${GREEN} contrôle du volume  $NC Super + v"
echo -e " ${GREEN} aide                $NC Super + h"
echo ""
echo -e " ${BLUE} chande de disposition $NC Alt + space"
echo ""
echo -e " ${CYAN} client préc/suiv      $NC Alt + k/j or Tab"
echo -e " ${CYAN} tag occupé préc/suiv  $NC Alt + Left/Right"
echo -e " ${CYAN} tag préc/suiv         $NC Alt + Up/Down"
echo -e " ${CYAN} dernier tag occupé    $NC Alt + a"
echo ""
echo -e " ${CYAN} envoyer au tag 'n'     $NC Alt + Shift + [1..n]"
echo -e " ${CYAN} aller au tag 'n'       $NC Alt + [1..n]"
echo -e " ${CYAN} changer de region      $NC Alt + Shift + Left/Right"
echo -e " ${CYAN} libère/tile le client  $NC Alt + t"
echo -e " ${CYAN} minimise le client     $NC Alt + w"
echo -e " ${CYAN} rétablit le client     $NC Alt + Shift + w"
echo -e " ${CYAN} recherche un client    $NC Alt + f"
echo ""
echo ""
echo -e " ${green} manuel complet avec [man spectrwm]"
echo -en " ${green} presser Enter pour quitter..."
read anykey
exit 0
