#! /bin/bash
### BEGIN INIT INFO
# Provides:          firewall
# Required-Start:
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: launch firewall
# Description:       clean iptables rules then reload iptables
### END INIT INFO

# Author : arpinux <contact@arpinux.org>

. /lib/lsb/init-functions

# Le switch case ci-dessous permet de savoir si le système souhaite lancer ou arrêter le script (on le lance au démarrage et l'arrête à la fermeture du système)
case "$1" in
    start)
        log_action_msg "Basic Firewall: nettoyage des rules"
        iptables -F
        iptables -Z
        iptables -X
        log_action_msg "Basic Firewall: activation des rules iptables"
        iptables-restore < /etc/firewall.rules
        ip6tables-restore < /etc/firewall6.rules
        ;;
    restart)
        log_action_msg "Basic Firewall: nettoyage des rules"
        iptables -F
        iptables -Z
        iptables -X
        log_action_msg "Basic Firewall: activation des rules iptables"
        iptables-restore < /etc/firewall.rules
        ip6tables-restore < /etc/firewall6.rules
        ;;
    stop)
        iptables -F
        iptables -Z
        iptables -X
        log_action_msg "Basic Firewall: nettoyage des rules"
        ;;
    status)
        log_action_msg "Basic Firewall: rules iptables en cours"
        iptables -L
        ;;
    *)
        echo 'Utilisation: /etc/init.d/firewall.sh {start|restart|stop|status}'
        exit 1
        ;;
esac
 
exit 0
