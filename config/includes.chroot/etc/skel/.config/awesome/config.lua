-- fichier minimal de configuration pour awesome -----------------------
-- vous permet de: - définir votre dossier de captures
--                 - afficher l'icone de menu
--                 - sélectionner la langue du menu
--                 - définir l'apparence de la liste de tags
--                 - définir la barre de titre
--                 - configurar la barre de statut (type & position)
--                 - définir le format de la date
--                 - activer/désactiver les widgets
------------------------------------------------------------------------

-- dossier des captures d'écrans
screenshots = home .. "/tmp/"

-- icones de menu awesome
menu_icon = true -- si true, affiche l'icone de menu awesome à gauche de la barre, sinon, le menu reste accessible au clic-droit ou Alt+p
menu_lang = "fr" -- supporte fr ou en

-- taglists
taglist = "static" -- supporte static/dynamic
-- si static sélectionner taglists_type
taglist_style = "arabic" -- supporte arabic (1,2,3...),east_arabic (١, ٢, ٣,...), persian_arabic(٠,١,٢,٣,۴,....}, roman (I, II, III, IV,)

-- clients
enable_titlebar = false      -- affiche la barre de titre des clients
enable_floatbar = false      -- affiche la barre de titre des clients libres
tasklist_icon_enable = false -- affiche l'icone dans la barre de titre

-- barre de widgets
widget_mode = "simple"     -- type de widgets, supporte simple/arrow
wibox_position = "bottom" -- position e la barre, supporte top/bottom
wibox_opacity = 1         -- opacité de la barre entre 0 et 1

-- widget date
date_format = "%b,%d %H:%M" -- man date pour plus de détails

-- widget cpu
cpu_enable = true     -- affiche l'utilisation cpu
cputemp_enable = true -- affiche la température cpu

-- widget mem
mem_enable = true -- affiche l'utilisation mémoire

-- widget disk
diskroot_enable = true -- affiche l'utilisation de la partition /

-- widget system
uptime_enable = false -- affiche l'uptime et le load cpu

-- widget réseau
net_enable = true      -- affiche le traffic réseau
apt_enable = true      -- affiche les mises à jour disponibles (dépend de apt-show-versions)
gmail_enable = false   -- affiche le nombre de mail non lus dans gmails (dépend de ~/.netrc indiquant -> machine mail.google.com login <e-mail address> password <password>)
weather_enable = false -- affiche les infos météo
weather_code = "LFPO"  -- code de région pour la météo

-- widget volume
vol_enable = true -- affiche le volume

-- widget batterie
battery_enable = true -- affiche l'état de la batterie

-- widget musique
moc_enable = true   -- affiche les infos moc player
cmus_enable = false -- affiche les infos cmus player

--Aphelion&arpinux@2013-------------------------------------------------
